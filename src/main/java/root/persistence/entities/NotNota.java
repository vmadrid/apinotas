/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Vito
 */
@Entity
@Table(name = "not_notas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotNota.findAll", query = "SELECT n FROM NotNota n")
    , @NamedQuery(name = "NotNota.findByNotRut", query = "SELECT n FROM NotNota n WHERE n.notRut = :notRut")
    , @NamedQuery(name = "NotNota.findByNotNota1", query = "SELECT n FROM NotNota n WHERE n.notNota1 = :notNota1")
    , @NamedQuery(name = "NotNota.findByNotNota2", query = "SELECT n FROM NotNota n WHERE n.notNota2 = :notNota2")
    , @NamedQuery(name = "NotNota.findByNotNota3", query = "SELECT n FROM NotNota n WHERE n.notNota3 = :notNota3")})
public class NotNota implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "not_rut")
    private String notRut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "not_nota1")
    private double notNota1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "not_nota2")
    private double notNota2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "not_nota3")
    private double notNota3;

    public NotNota() {
    }

    public NotNota(String notRut) {
        this.notRut = notRut;
    }

    public NotNota(String notRut, double notNota1, double notNota2, double notNota3) {
        this.notRut = notRut;
        this.notNota1 = notNota1;
        this.notNota2 = notNota2;
        this.notNota3 = notNota3;
    }

    public String getNotRut() {
        return notRut;
    }

    public void setNotRut(String notRut) {
        this.notRut = notRut;
    }

    public double getNotNota1() {
        return notNota1;
    }

    public void setNotNota1(double notNota1) {
        this.notNota1 = notNota1;
    }

    public double getNotNota2() {
        return notNota2;
    }

    public void setNotNota2(double notNota2) {
        this.notNota2 = notNota2;
    }

    public double getNotNota3() {
        return notNota3;
    }

    public void setNotNota3(double notNota3) {
        this.notNota3 = notNota3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notRut != null ? notRut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotNota)) {
            return false;
        }
        NotNota other = (NotNota) object;
        if ((this.notRut == null && other.notRut != null) || (this.notRut != null && !this.notRut.equals(other.notRut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.NotNota[ notRut=" + notRut + " ]";
    }
    
}
