
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.NotNota;

@Path("/notas")
public class NotasREST {
    EntityManagerFactory emf=Persistence.createEntityManagerFactory("UP_notas");
    EntityManager em;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(){
        em=emf.createEntityManager();
        List<NotNota> lista=em.createNamedQuery("NotNota.findAll").getResultList();
        return Response.ok().entity(lista).build();
    }
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar){
        em=emf.createEntityManager();
        NotNota nota=em.find(NotNota.class,idbuscar);
        em.close();
        return Response.ok(500).entity(nota).entity(nota).build();
    }
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevo(NotNota notaNueva){
        em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(notaNueva);
        em.getTransaction().commit();
        em.close();
        return "Registro de notas guardado";
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(NotNota notaUpdate){
        em=emf.createEntityManager();
        em.getTransaction().begin();
        notaUpdate=em.merge(notaUpdate);
        em.getTransaction().commit();
        em.close();
        return Response.ok(notaUpdate).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces({MediaType.TEXT_PLAIN,MediaType.APPLICATION_JSON})
    public Response eliminaId(@PathParam("iddelete") String iddelete){
        em=emf.createEntityManager();
        em.getTransaction().begin();
        NotNota notaEliminar=em.getReference(NotNota.class, iddelete);
        em.remove(notaEliminar);
        em.getTransaction().commit();
        em.close();
        return Response.ok("Registro de notas eliminado").build();
    }
}
